$(document).ready(function() {
    //tabs
    $(".tabs li").click(function(e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.hasClass('active')) return ;
        $(".tabs li.active").removeClass("active");
        $this.addClass("active");
        $(".tabs__content.active").removeClass('active');
        var activeTab = $this.find("a").attr("href");
        $(activeTab).addClass('active');
    });


    //menu
    $('.nav-bar__btn').on('click', function(e){
        $('.main-menu').toggleClass('show');
    });

    $('.main-menu__item').on('click', function(e){
        $('.main-menu__item.active').removeClass('active');
        var $menuItem = $(e.currentTarget);
        $menuItem.addClass('active');
        $('.nav-bar__btn-text').text($menuItem.text());
        $('.main-menu').removeClass('show');
    });

    //add options
    var checkedList = [];
    var MAX_CHECKED_OPTIONS = 3;

    function generateOptions() {
        var fragment = document.createDocumentFragment();
        for(var i=0; i<100; i++) {
            fragment.appendChild(renderOption(i));
        }
        $('.options-list')[0].appendChild(fragment);
    };

    function renderOption(index){
        var label = document.createElement('label');
        label.className = 'custom-checkbox';
        var checkbox = document.createElement('input');
        checkbox.type = 'checkbox';
        checkbox.onchange = onChangeOption;
        var textNode =  document.createTextNode('Option' + (index + 1));
        var span =  document.createElement('span');
        label.appendChild(checkbox);
        label.appendChild(span);
        label.appendChild(textNode);
        return label;
    };

    function onChangeOption(e) {
        if (e.currentTarget.checked) {
            checkedList.push(e.currentTarget);
            if (checkedList.length > MAX_CHECKED_OPTIONS) {
                var option = checkedList.splice(0, 1)[0];
                option.checked = false;
            }
        } else {
            var position = checkedList.indexOf(e.currentTarget);
            if (position >= 0) {
                checkedList.splice(position, 1);
            }
        }
    }

    generateOptions();

    $('.options-more').click(function() {
        $('.options-list').toggleClass('show');
    })

});